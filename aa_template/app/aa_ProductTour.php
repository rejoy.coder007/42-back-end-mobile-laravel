<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aa_ProductTour extends Model
{

    public function scopeGiveRandomLikes($query)
    {
        return $query->inRandomOrder()->take(4);
    }
}
