<?php

namespace App\Http\Controllers\ab_Apparel;

use App\aa_ProductApparel;
use App\aa_ProductTour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPUnit\Framework\Constraint\Count;

class aa_Apparel extends Controller
{

    public function get_random_carousel()
    {
        //

        $random_carousel = aa_ProductApparel::inRandomOrder()->take(4)->get();




        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
       // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($random_carousel); $i++) {


            $val["image".$i] =$random_carousel[$i]->image_url;
        }













        $data["results"][]=$val;


        return response()->json($data);
    }



    public function get_random_carousel_2()
    {
        //

        $random_carousel = aa_ProductTour::inRandomOrder()->take(4)->get();




        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($random_carousel); $i++) {


            $val["image".$i] =$random_carousel[$i]->image_url;
        }













        $data["results"][]=$val;


        return response()->json($data);
    }



    public function get_random_Book(Request $request)
    {
        //

        $booking =null;
        $booking = \App\aa_Booking_App::where('item_id', $request->id)->first();

        $val = array();
        $data = array();

        if($booking!=null)
        {
            $val["OK"]=3;

            $data["results"][]=$booking;

        }



        return response()->json($data);

    }


    public function get_random_Cart(Request $request)
    {
        //

        $booking =null;
        $booking = \App\aa_Cart_App::where('item_id', $request->id)->first();

        $val = array();
        $data = array();

        if($booking!=null)
        {
            $val["OK"]=3;

            $data["results"][]=$booking;

        }



        return response()->json($data);

    }

}
