<?php

namespace App\Http\Controllers\ab_Tour;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAccessController extends Controller
{
    public function view_package( Request $request)
    {

        $packages =null;
        $packages = \App\aa_ProductTour::all();



        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($packages); $i++) {


            $val["package_name".$i] =$packages[$i]->package_name;
            $val["Duration".$i] =$packages[$i]->duration;
            $val["image_url".$i] =$packages[$i]->image_url;
            $val["details".$i] =$packages[$i]->details;
            $val["per_head_price".$i] =$packages[$i]->per_head_price;
            $val["description".$i] =$packages[$i]->description;




        }













        $data["results"][]=$val;


        return response()->json($data);


    }

    public function bookpackage( Request $request)
    {




            $user = new \App\aa_booking_tour();
            $user->user_id = $request->user_id;
            $user->package_id = $request->package_id;

            $user->save();


            $val = array();
            $data = array();
            $val["OK"]=1;

           $val["msg"]="Success";
            $data["results"][]=$val;





        return response()->json($data);











    }


    public function showbookallpackage( Request $request)
    {




        $users =null;
        $users = \App\aa_booking_tour::all();



        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($users); $i++) {

            $val["id".$i] =$users[$i]->id;
            $val["user_id".$i] =$users[$i]->user_id;
            $val["package_id".$i] =$users[$i]->package_id;


            $val["name".$i] = \App\User::where('id',$users[$i]->user_id)->first()->name;
           $val["package_name".$i] =   \App\aa_ProductTour::where('id',$users[$i]->package_id)->first()->package_name;
            //$val["package_name".$i] =   \App\aa_ProductTour::where('id',$users[$i]->package_id)->first();

        }













        $data["results"][]=$val;


        return response()->json($data);










    }


    public function showbookpackageid( Request $request)
    {


        //return response()->json($request->all());

        $booking =null;
        $booking = \App\aa_booking_tour:: where('user_id', $request->id)->get();
       // $users = \App\aa_booking_tour:: all();
       // return response()->json( $users);

        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($booking); $i++) {

            $val["id".$i] =$booking[$i]->id;
            $val["user_id".$i] =$booking[$i]->user_id;
            $val["package_id".$i] =$booking[$i]->package_id;


            $val["name".$i] = \App\User::where('id',$booking[$i]->user_id)->first()->name;
            $val["package_name".$i] =   \App\aa_ProductTour::where('id',$booking[$i]->package_id)->first()->package_name;
        }













        $data["results"][]=$val;


        return response()->json($data);










    }


    public function del_id( Request $request)
    {


        $user_d =null;
        $val = array();
        $data = array();

        $user_d = \App\aa_booking_tour::find($request->id);


        if($user_d!=null)
        {

            $user_d->delete();



            $val["OK"]=1;
            $data["results"][]=$val;




        }
        else{


            $val["OK"]=0;
            $data["results"][]=$val;

        }


        return response()->json($data);










    }




    public function AddToCart( Request $request)
    {




        $user = new \App\aa_Cart_tour();
        $user->user_id = $request->user_id;
        $user->package_id = $request->package_id;

        $user->save();


        $val = array();
        $data = array();
        $val["OK"]=1;
        $data["results"][]=$val;





        return response()->json($data);











    }





    public function ShowAllCartById( Request $request)
    {


        //return response()->json($request->all());

        $booking =null;
        $booking = \App\aa_Cart_tour:: where('user_id', $request->user_id)->get();
        // $users = \App\aa_booking_tour:: all();
        // return response()->json( $users);

        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($booking); $i++) {

            $val["id".$i] =$booking[$i]->id;
            $val["user_id".$i] =$booking[$i]->user_id;
            $val["package_id".$i] =$booking[$i]->package_id;


            $val["name".$i] = \App\User::where('id',$booking[$i]->user_id)->first()->name;
            $val["package_name".$i] =   \App\aa_ProductTour::where('id',$booking[$i]->package_id)->first()->package_name;
        }













        $data["results"][]=$val;


        return response()->json($data);










    }


    public function DeleteCart( Request $request)
    {


        $user_d =null;
        $val = array();
        $data = array();

        $user_d = \App\aa_Cart_tour::find($request->id);


        if($user_d!=null)
        {

            $user_d->delete();



            $val["OK"]=1;
            $data["results"][]=$val;




        }
        else{


            $val["OK"]=0;
            $data["results"][]=$val;

        }


        return response()->json($data);










    }



}
