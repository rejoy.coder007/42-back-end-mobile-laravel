<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aa_Booking_App extends Model
{
    protected $fillable = [
        'user_id', 'package_id'
    ];
}
