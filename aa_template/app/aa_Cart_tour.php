<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aa_Cart_tour extends Model
{
    protected $fillable = [
        'user_id', 'package_id'
    ];
}
