html
{
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
}

*, *:before, *:after
{
      -webkit-box-sizing: inherit;
      box-sizing: inherit;
}

a
{
      color: #212121;
      text-decoration: none;
}

a:visited
{
      color: #212121;
}

a:hover
{
      color: #878787;
}

.logo a
{
      color: #e9e9e9;
}

h1
{
      margin-bottom: 40px;
}

h2
{
      margin-bottom: 10px;
}

img
{
      max-width: 100%;
}

.clearfix::after
{
      clear: both;
      content: '';
      display: table;
}

.container
{
      margin: auto;
      max-width: 1200px;
}

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video
{
      border: 0;
      font: inherit;
      font-size: 100%;
      margin: 0;
      padding: 0;
      vertical-align: baseline;
}

article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section
{
      display: block;
}

body
{
      line-height: 1;
}

ol, ul
{
      list-style: none;
}

blockquote, q
{
      quotes: none;
}

blockquote:before, blockquote:after,
q:before, q:after
{
      content: '';
      content: none;
}

table
{
      border-collapse: collapse;
      border-spacing: 0;
}

body
{
      font-family: 'Roboto', Arial, sans-serif;
      font-size: 18px;
      font-weight: 300;
      line-height: 1.6;
}

h1, h2, .product-section-subtitle, .product-section-price
{
      font-family: 'Montserrat', Arial, sans-serif;
      font-weight: bold;
}

h1
{
      font-size: 38px;
      line-height: 1.2;
}

h1.stylish-heading
{
      margin-bottom: 60px;
      position: relative;
}

h1.stylish-heading:before, h1.stylish-heading:after
{
      background: #212121;
      content: '';
      display: block;
      height: 1px;
      left: 0;
      position: absolute;
      top: -4px;
      width: 66px;
}

h1.stylish-heading:after
{
      bottom: -14px;
      top: auto;
}

h2
{
      font-size: 22px;
}

.text-center
{
      text-align: center;
}

.spacer
{
      margin-bottom: 30px;
}

.sticky-footer
{
      -ms-flex-direction: column;
      -webkit-box-direction: normal;
      -webkit-box-orient: vertical;
      -webkit-flex-direction: column;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      flex-direction: column;
      min-height: 100vh;
}

.full-width
{
      width: 100%;
}

.breadcrumbs
{
      -webkit-background-size: cover;
      background-image: -webkit-gradient(linear, left top, right bottom, from(rgba(255, 0, 0, 0.8)), to(rgba(40, 180, 133, 0.1))), url("/img/aa_header/aa_bg.jpg");
      background-image: -webkit-linear-gradient(left top, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-image: -o-linear-gradient(left top, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-image: linear-gradient(to right bottom, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-size: cover;
      border-bottom: 1px solid #CDCDCD;
      padding: 24px 0;
}

.breadcrumbs .breadcrumb-separator
{
      color: #545454;
      font-size: 14px;
}

.button
{
      border: 1px solid #212121;
      padding: 12px 40px;
}

.button:hover
{
      background: #212121;
      color: #e9e9e9;
}

.button-white
{
      border: 1px solid #e9e9e9;
      color: #e9e9e9 !important;
}

.button-white:hover
{
      background: #e9e9e9;
      color: #212121 !important;
}

.button-primary
{
      background: #3EBFA4;
      color: white !important;
      padding: 12px 40px;
}

.button-primary:hover
{
      background: #35a48d;
}

.button-plain
{
      background: transparent;
      border: 1px solid #212121 !important;
}

.button-container
{
      margin: 80px 0;
}

.section-description
{
      margin: 44px auto;
      width: 80%;
}

.sidebar h3
{
      font-weight: bold;
      margin-bottom: 16px;
}

.sidebar ul
{
      line-height: 2;
      margin-bottom: 20px;
}

.might-like-section
{
      background: #F5F5F5;
      padding: 40px 0 70px;
}

.might-like-section h2
{
      padding-bottom: 30px;
}

.might-like-section .might-like-grid
{
      -ms-grid-columns: 1fr 1fr 1fr 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr 1fr 1fr 1fr;
}

.might-like-section .might-like-product
{
      background: white;
      border: 1px solid #979797;
      padding: 30px 0 20px;
      text-align: center;
}

.might-like-section .might-like-product img
{
      width: 70%;
}

.might-like-section .might-like-product-price
{
      color: #919191;
}

form .half-form
{
      -ms-grid-columns: 1fr 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr 1fr;
}

form button[type="submit"]
{
      border-style: none;
      cursor: pointer;
      font-size: 18px;
      line-height: 1.6;
}

.form-group
{
      margin-bottom: 20px;
}

.form-group label
{
      display: block;
}

.form-group input
{
      font-size: 16px;
      padding: 12px;
      width: 100%;
}

header.with-background
{
      -webkit-background-size: cover;
      background: url("/img/triangles.svg");
      background-size: cover;
      color: #e9e9e9;
}

header.with-background .top-nav
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      letter-spacing: 1.5px;
      padding: 40px 0;
}

header.with-background .top-nav .logo
{
      font-size: 28px;
      font-weight: bold;
}

header.with-background .top-nav ul
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      text-transform: uppercase;
      width: 40%;
}

header.with-background .top-nav ul a
{
      color: #e9e9e9;
}

header.with-background .top-nav ul a:hover
{
      color: #d0d0d0;
}

header.with-background .hero
{
      -ms-grid-columns: 1fr 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr 1fr;
      padding-bottom: 84px;
      padding-top: 20px;
}

header.with-background .hero .hero-image
{
      padding-left: 60px;
}

header.with-background .hero h1
{
      font-size: 52px;
      margin-top: 50px;
}

header.with-background .hero p
{
      margin: 40px 0 68px;
}

header.with-background .hero .button
{
      margin-right: 14px;
}

header
{
      -webkit-background-size: cover;
      background-image: -webkit-gradient(linear, left top, right bottom, from(rgba(255, 0, 0, 0.8)), to(rgba(40, 180, 133, 0.1))), url("/img/aa_header/aa_bg.jpg");
      background-image: -webkit-linear-gradient(left top, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-image: -o-linear-gradient(left top, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-image: linear-gradient(to right bottom, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-size: cover;
}

header .top-nav
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      letter-spacing: 1.5px;
      padding: 30px 0;
}

header .top-nav .logo
{
      color: #e9e9e9;
      font-size: 28px;
      font-weight: bold;
}

header .top-nav ul
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      font-weight: 400;
      justify-content: space-between;
      padding-top: 8px;
      text-transform: uppercase;
      width: 40%;
}

header .top-nav ul a
{
      color: #e9e9e9;
}

header .top-nav ul a:hover
{
      color: #d0d0d0;
}

header .top-nav ul .cart-count
{
      -webkit-border-radius: 50%;
      background: #FFD94D;
      border-radius: 50%;
      color: #212121;
      display: inline-block;
      font-size: 14px;
      line-height: 0;
}

header .top-nav ul .cart-count span
{
      display: inline-block;
      margin-left: 6px;
      margin-right: 6px;
      padding-bottom: 50%;
      padding-top: 50%;
}

footer
{
      background: #535353;
      color: #e9e9e9;
      padding: 40px 0;
}

.footer-content
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
}

.footer-content .heart
{
      color: #FFBABA;
}

.footer-content ul
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      width: 30%;
}

.footer-content a
{
      color: #e9e9e9;
}

h1.checkout-heading
{
      margin-top: 40px;
}

.checkout-section
{
      -ms-grid-columns: 1fr 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr 1fr;
      margin: 40px auto 80px;
}

.checkout-section .checkout-table-container
{
      margin-left: 124px;
}

.checkout-section h2
{
      margin-bottom: 28px;
}

.checkout-section .checkout-table-row
{
      -ms-flex-align: center;
      -ms-flex-pack: justify;
      -webkit-align-items: center;
      -webkit-box-align: center;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      align-items: center;
      border-top: 1px solid #919191;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      padding: 14px 0;
}

.checkout-section .checkout-table-row:last-child
{
      border-bottom: 1px solid #919191;
}

.checkout-section .checkout-table-row .checkout-table-row-left, .checkout-section .checkout-table-row .checkout-table-row-right
{
      -ms-flex-align: center;
      -ms-flex-pack: justify;
      -webkit-align-items: center;
      -webkit-box-align: center;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      align-items: center;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
}

.checkout-section .checkout-table-row .checkout-table-row-left
{
      width: 75%;
}

.checkout-section .checkout-table-row .checkout-table-img
{
      max-height: 60px;
}

.checkout-section .checkout-table-row .checkout-table-description
{
      color: #919191;
}

.checkout-section .checkout-table-row .checkout-table-price
{
      padding-top: 6px;
}

.checkout-section .checkout-table-row .checkout-table-quantity
{
      border: 1px solid #919191;
      margin-right: 5px;
      padding: 4px 12px;
}

.checkout-section .checkout-totals
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      border-bottom: 1px solid #919191;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      line-height: 2;
      padding: 18px 0;
}

.checkout-section .checkout-totals .checkout-totals-right
{
      text-align: right;
}

.checkout-section .checkout-totals .checkout-totals-total
{
      font-size: 22px;
      font-weight: bold;
      line-height: 2.2;
}

.StripeElement
{
      -o-transition: box-shadow 150ms ease;
      -webkit-border-radius: 4px;
      -webkit-box-shadow: 0 1px 3px 0 #e6ebf1;
      -webkit-box-sizing: border-box;
      -webkit-transition: box-shadow 150ms ease;
      -webkit-transition: -webkit-box-shadow 150ms ease;
      background-color: white;
      border: 1px solid transparent;
      border-radius: 4px;
      box-shadow: 0 1px 3px 0 #e6ebf1;
      box-sizing: border-box;
      height: 40px;
      padding: 10px 12px;
      transition: -webkit-box-shadow 150ms ease;
      transition: box-shadow 150ms ease;
      transition: box-shadow 150ms ease, -webkit-box-shadow 150ms ease;
}

.StripeElement--focus
{
      -webkit-box-shadow: 0 1px 3px 0 #cfd7df;
      box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid
{
      border-color: #fa755a;
}

.StripeElement--webkit-autofill
{
      background-color: #fefde5 !important;
}

@media only screen and (max-width: 1200px)
{
      .container
      {
            max-width: 960px;
      }
}

@media only screen and (max-width: 992px)
{
      header.with-background .top-nav, header .top-nav
      {
            -ms-flex-direction: column;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            -webkit-flex-direction: column;
            flex-direction: column;
      }

      header.with-background .top-nav .logo, header .top-nav .logo
      {
            margin: auto;
      }

      header.with-background .top-nav ul, header .top-nav ul
      {
            margin: 20px auto 0;
            width: 80%;
      }

      header.with-background .hero, header .hero
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
            text-align: center;
      }

      header.with-background .hero .hero-image, header .hero .hero-image
      {
            margin-top: 40px;
            padding-left: 0;
      }

      .featured-section
      {
            padding: 50px 0;
      }

      .featured-section .products
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .blog-section .blog-posts
      {
            -ms-grid-columns: 1fr;
            grid-template-areas: "blog3" "blog2" "blog1";
            grid-template-columns: 1fr;
            text-align: center;
      }

      .blog-section .blog-posts .blog-post
      {
            margin-top: 30px;
      }

      .footer-content
      {
            -ms-flex-direction: column;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            -webkit-flex-direction: column;
            flex-direction: column;
      }

      .footer-content .made-with
      {
            margin: auto;
      }

      .footer-content ul
      {
            margin: 20px auto;
            width: 60%;
      }

      .breadcrumbs
      {
            text-align: center;
      }

      .products-section
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .products-section .sidebar
      {
            text-align: center;
      }

      .products-section .products
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .product-section
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .might-like-section .might-like-grid
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .cart-section
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .cart-section .cart-buttons
      {
            -ms-flex-direction: column;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            -webkit-flex-direction: column;
            flex-direction: column;
            text-align: center;
      }

      .cart-section .cart-table-row .cart-table-row-left
      {
            width: 30%;
      }

      .cart-section .cart-table-row .cart-table-row-right
      {
            width: 55%;
      }

      .cart-section .cart-table-row img
      {
            display: none;
      }

      .cart-section .have-code-container
      {
            width: 100%;
      }

      .cart-section .cart-totals
      {
            -ms-flex-direction: column;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            -webkit-flex-direction: column;
            flex-direction: column;
      }

      .cart-section .cart-totals .cart-totals-left
      {
            margin-bottom: 20px;
            width: 100%;
      }

      .cart-section .cart-totals .cart-totals-right
      {
            width: 100%;
      }

      .checkout-section
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .checkout-section .checkout-table-container
      {
            margin-left: 10px;
      }
}




