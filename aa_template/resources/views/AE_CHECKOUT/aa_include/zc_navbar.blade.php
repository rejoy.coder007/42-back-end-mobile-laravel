<header>
    <div class="top-nav container">
        <div class="logo"><a href="/">Hardware Shop</a></div>
            <ul>
                <li><a href="{{ route('shop.full') }}">Shop</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="{{ route('cart.index') }}">Cart <span class="cart-count">

                            @if(Cart::instance('default')->count()>0)
                                <span>{{Cart::instance('default')->count() }}</span>
                            @endif


                        </span></a></li>
            </ul>

    </div> <!-- end top-nav -->
</header>
