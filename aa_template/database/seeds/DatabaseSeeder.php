<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(aa_ApparelBooking::class);

        $this->call(aa_UserTable::class);

        $this->call(aa_TourProductSeeder::class);
        $this->call(aa_ApparelProductSeeder::class);
    }


}
