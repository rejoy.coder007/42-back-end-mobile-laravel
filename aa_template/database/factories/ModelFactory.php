<?php

$unique_No=0;

$factory->define(App\User::class, function (Faker\Generator $faker) {

    $row = array();
    switch ( aa_UserTable::$count)
    {
        case 0:

            $row = [
                'name' => "rambo",
                'email' => "a@a.com",
                'admin' => 1,
                'address' =>$faker->address,

                'password' => bcrypt("a"),

            ];

        break;


        case 1:

            $row = [
                'name' => "remo",
                'email' => "b@b.com",
                'admin' => 0,
                'address' =>$faker->address,

                'password' => bcrypt("b"),

            ];

        break;

        default:

            $row = [
                'name' => $faker->name,
                'email' => $faker->safeEmail,
                'address' =>$faker->address,
                'admin' => 0,

                'password' => bcrypt($faker->password),

            ];

        break;
    }


    aa_UserTable::$count++;

    return $row;








});

$factory->define(App\aa_ProductWeb::class, function (Faker\Generator $faker) {


  $random_int = random_int(0,100);

    $input = array("Laptop", "Desktop", "Printer", "Webcam","Hardisk");

    $rand_item= array_rand($input, 1);

    $item_name = $input[$rand_item]." ".$random_int;

    // 'category_id' => $category_id
    //

    /*
    $category_id =0;

    switch ($rand_item)
    {
        case "Laptop":
            $category_id=0;
            break;
        case "Desktop":
            $category_id=1;
            break;
        case "Printer":
            $category_id=2;
            break;
        case "Webcam":
            $category_id=3;
            break;

        case "Hardisk":
            $category_id=4;
            break;
    }
*/


    return [
        'name' => $item_name,
        'slug' => str_slug($item_name, "-"),
        'details' => $faker->word,
        'price' => random_int(50000,100000),
        'description' => $faker->text,

    ];


});

$factory->define(App\aa_ProductTour::class, function (Faker\Generator $faker) {

    $input = array("Kuruva Package", "Pookode Package", "Bansura Package", "Thirunelli","Muthanga");

    $img_url =  "/images/Tour/product_".aa_TourProductSeeder::$count.".jpeg";

    $item_name = $input[aa_TourProductSeeder::$count];

    $old_price = random_int(2000,5000);

    $new_price =  random_int($old_price-100,$old_price-10);

    aa_TourProductSeeder::$count++;
    return [
        'package_name' => $item_name,
        'duration' => random_int(10,30),
        'details' => $faker->word,
        'image_url'=> $img_url,
        'per_head_price_old' => $old_price,
        'per_head_price_new' => $new_price,
        'description' => $faker->text,

    ];

});

$factory->define(App\aa_ProductApparel::class, function (Faker\Generator $faker) {


    $input = array("T shirt", "Pants", "Formal", "frock","sari");

    $size = array("XXL", "XL");




    $rand_size= array_rand($size, 1);
    $old_price = random_int(2000,5000);

    $new_price =  random_int($old_price-100,$old_price-10);


    $img_url =  "/images/Apparel/product_".aa_ApparelProductSeeder::$count.".jpeg";



    $item_name = $input[aa_ApparelProductSeeder::$count];

    aa_ApparelProductSeeder::$count++;
    return [

        'cloth_name' =>$item_name,
        'size' => $size[$rand_size],
        'details' => $faker->word,
        "image_url"=>$img_url,
        'new_price' => $new_price,
        'old_price' => $old_price,
        'description' => $faker->text,
    ];
});
$factory->define(App\aa_Cart_App::class, function (Faker\Generator $faker) {
    return [
        'new_price' => $faker->randomNumber(),
        'old_price' => $faker->randomNumber(),
        /*
        'description' => $faker->text,
        'item_id' =>random_int(0,10),*/
    ];
});

$factory->define(App\aa_Booking_App::class, function (Faker\Generator $faker) {
    return [
        'new_price' => $faker->randomNumber(),
        'description' => $faker->text,
        'item_id' => random_int(0,10),
    ];
});

